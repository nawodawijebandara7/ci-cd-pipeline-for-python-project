Flask==2.2.2
py-cpuinfo==9.0.0
psutil==5.9.4
gunicorn==20.1.0
black==22.12.0 
flake8==6.0.0
pytest==7.1.1
